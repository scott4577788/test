namespace Rewards4Racing.Data
{
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class PeopleContext : DbContext
    {
      
        public PeopleContext()
            : base("name=PeopleContext")
        {
        }


        public virtual DbSet<Person> Persons { get; set; }
    }

}