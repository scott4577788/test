﻿using Rewards4Racing.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace PeopleWebApp.Controllers
{
    [Authorize(Roles = "Admin")]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var people = GetPeopleByFirstNameAscending();

            return View(people);
        }

        [HttpPost]
        public ActionResult DeletePerson(string personId)
        {
            using (var db = new PeopleContext())
            {
                var dbPerson = db.Persons.Find(personId);
                db.Persons.Remove(dbPerson);
                db.SaveChanges();
            }

            return Json("Success");
        }

        private List<Person> GetPeopleByFirstNameAscending()
        {
            using (var db = new PeopleContext())
            {
                return db.Persons.OrderBy(p => p.first_name).ToList();
            }
        }
    }
}