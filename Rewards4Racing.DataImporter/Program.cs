﻿using CsvHelper;
using Rewards4Racing.Data;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Rewards4Racing.DataImporter
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> people = GetBritishPeopleWithValidEmail();

            people = ConvertEmailToLowerCase(people);
            people = CapitaliseNames(people);
            DisplayPeople(people);
            ImportPeople(people);
            Console.ReadLine();
        }

        private static void ImportPeople(List<Person> people)
        {
            try
            {
                using (var db = new PeopleContext())
                {
                    if (db.Persons.Any())
                    {
                        Console.WriteLine("Data already imported.");
                        return;
                    }
                    db.Persons.AddRange(people);

                    if (db.SaveChanges() > 0)
                    {
                        Console.WriteLine("Data imported successfully");
                    }
                    else
                    {
                        Console.WriteLine("Data could not be imported.");
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Data could not be imported due to an error. " + e.Message);
            }
        }

        private static List<Person> GetBritishPeopleWithValidEmail()
        {
            using (var sr = new StreamReader(@"CSVData.csv"))
            {
                var reader = new CsvReader(sr);

                return reader.GetRecords<Person>().Where(p => p.country.Equals("United Kingdom", StringComparison.OrdinalIgnoreCase) && IsValidEmail(p.email))
                                                  .ToList();
            }
        }

        private static bool IsValidEmail(string email)
        {
            string regexPattern = @"^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$";
            Match matches = Regex.Match(email, regexPattern);

            return matches.Success;
        }

        private static List<Person> CapitaliseNames(List<Person> people)
        {
            people.ForEach(p =>
            {
                p.first_name = p.first_name.UppercaseFirstLetter();
                p.last_name = p.last_name.UppercaseFirstLetter();
            });

            return people;
        }

        private static List<Person> ConvertEmailToLowerCase(List<Person> people)
        {
            people.ForEach(p => p.email = p.email.ToLower());

            return people;
        }

        private static void DisplayPeople(List<Person> people)
        {
            foreach (Person person in people)
            {
                Console.WriteLine("{0} {1}, {2}, {3}, {4}", person.id, person.first_name, person.last_name,
                person.country, person.email);
            }
        }
    }
}
